import {HttpResponse} from "./HttpResponse";
import {Response} from "./Response";
import {ResponseFactory} from "./ResponseFactory";

/**
 * Default implementation of a ResponseFactory.
 */
export class HttpResponseFactory implements ResponseFactory {
    /**
     * @inheritDoc
     */
    public create(body: string, status: number, contentType: string | null): Response {
        return new HttpResponse(body, status, contentType);
    }
}
