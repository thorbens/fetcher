import MIMEType from "whatwg-mimetype";

/**
 * Interface for a basic response.
 */
export interface Response {
    /**
     * The response body.
     */
    readonly body: string;
    /**
     * The status code for this response.
     */
    readonly status: number;
    /**
     * The content type for this response.
     */
    readonly contentType: string | null;

    /**
     * Returns the mime type for this response.
     */
    getMimeType(): MIMEType | null;

    /**
     * Retrieves the response body as JSON Object.
     *
     * @typeparam The type for the object after parsing.
     */
    asJSON<T extends object>(): T;
}
