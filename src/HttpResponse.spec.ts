import {HttpResponse} from "./HttpResponse";

interface TestResponseJson {
    foo: "bar";
}

describe("HttpResponse", () => {
    it("create the response correctly", async () => {
        const body = `{"foo":"bar"}`;
        const status = 200;
        const contentType = `application/json`;
        const response = new HttpResponse(body, status, contentType);

        expect(response.body).toEqual(body);
        expect(response.status).toEqual(status);
        expect(response.contentType).toEqual(contentType);

        const mimeType = response.getMimeType();
        expect(!!mimeType).toEqual(true);

        const responseJson = response.asJSON<TestResponseJson>();
        expect(!!responseJson).toEqual(true);
        expect(responseJson.foo).toEqual(`bar`);
    });
});
