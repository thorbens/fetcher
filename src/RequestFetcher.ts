import request from "request";
import {AbstractFetcher} from "./AbstractFetcher";
import {FetchOptions} from "./FetchOptions";
import {Response} from "./Response";

/**
 * Request implementation for a Fetcher.
 */
export class RequestFetcher extends AbstractFetcher {
    /**
     * @inheritDoc
     */
    public fetch(url: string, options?: FetchOptions): Promise<Response> {
        return this.performRequest(this.mapOptions(url, options || {}));
    }

    /**
     * Maps the fetch options to the actual lib options.
     *
     * @param url The url to fetch.
     * @param options The options to map.
     */
    private mapOptions(url: string, options: FetchOptions): request.OptionsWithUrl {
        const fetchOptions: request.OptionsWithUrl = {
            method: options.method || `get`,
            url,
        };
        if (options.headers) {
            fetchOptions.headers = options.headers;
        }
        if (options.data) {
            fetchOptions.headers = {
                "Content-Type": `application/json`,
                ...fetchOptions.headers, // merge with existing headers
            };
            fetchOptions.body = JSON.stringify(options.data);
        }
        if (options.formData) {
            fetchOptions.formData = options.formData;
        }

        return fetchOptions;
    }

    /**
     * Performs a request with the given options.
     *
     * @param options The options to use.
     */
    private performRequest(options: request.OptionsWithUrl): Promise<Response> {
        return new Promise<Response>((resolve, reject) => {
            request(options, (error, response, body) => {
                try {
                    const responseObject = this.processResponse(error, response, body);

                    responseObject.status >= 400 ? reject(response) : resolve(responseObject);
                } catch (e) {
                    reject(e);
                }
            });
        });
    }

    /**
     * Processes response arguments and creates a Response object.
     * If an error is present, this method rethrows the error.
     *
     * @param error The error text if an error occurred.
     * @param response The response.
     * @param body The response body.
     */
    private processResponse(error: string | undefined, response: any, body: string): Response {
        if (error) {
            throw new Error(error);
        }
        const contentType = response.headers["content-type"] || null;

        return this.getResponseFactory().create(body, response.statusCode, contentType);
    }
}
