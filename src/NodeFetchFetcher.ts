import fetch, {RequestInit} from "node-fetch";
import {AbstractFetcher} from "./AbstractFetcher";
import {FetchOptions} from "./FetchOptions";
import {Response} from "./Response";

/**
 * Node js implementation for a Fetcher (used node-fetch).
 */
export class NodeFetchFetcher extends AbstractFetcher {
    /**
     * @inheritDoc
     */
    public fetch(url: string, options?: FetchOptions): Promise<Response> {
        return this.performRequest(url, this.mapOptions(options || {}));
    }

    /**
     * Maps the given options to fetch options.
     *
     * @param options The options to map.
     */
    private mapOptions(options: FetchOptions): RequestInit {
        const requestInit: RequestInit = {
            method: options.method || `get`,
        };
        if (options.headers) {
            requestInit.headers = options.headers;
        }
        if (options.data) {
            requestInit.body = JSON.stringify(options.data);
            requestInit.headers = {
                "Content-Type": "application/json",
                ...requestInit.headers, // merge with existing headers
            };
        }

        return requestInit;
    }

    /**
     * Performs a request to the given url with the given options.
     *
     * @param url The url to fetch.
     * @param options The options to use.
     */
    private async performRequest(url: string, options: RequestInit): Promise<Response> {
        const res = await fetch(url, options);
        const contentType = res.headers.get("content-type") || null;
        const statusCode = res.status;
        const body = await res.text();
        const response = this.getResponseFactory().create(body, statusCode, contentType);
        if (response.status >= 400) {
            return Promise.reject(response);
        }

        return response;
    }
}
