import {FetchOptions, HttpResponseFactory, Method} from "./index";
import {NodeFetchFetcher} from "./NodeFetchFetcher";

interface HttpbinResponse {
    form: {
        foo: string,
    };
    data: string;
}

const fetcher = new NodeFetchFetcher(new HttpResponseFactory());

describe("NodeFetchFetcher", () => {

    it("should fetch the given url", async () => {
        const response = await fetcher.fetch("http://google.com");

        expect(!!response).toEqual(true);
        expect(response.status).toEqual(200);
    });

    it("should perform a post to the given url", async () => {
        const options: FetchOptions = {
            data: {
                foo: `bar`,
            },
            method: Method.POST,
        };
        const response = await fetcher.fetch("https://httpbin.org/post", options);

        expect(!!response).toEqual(true);
        expect(response.status).toEqual(200);

        const json = response.asJSON<HttpbinResponse>();
        expect(json.data).toEqual(JSON.stringify(options.data));
    });

    it("should reject not successful requests", async () => {
        let hasError = false;

        try {
            await fetcher.fetch("https://httpbin.org/status/404");
        } catch (e) {
            expect(!!e).toEqual(true);
            hasError = true;
        }

        expect(hasError).toEqual(true);
    });

    it("should set the headers for the request", async () => {
        const headers = {
            Foo: `bar`,
        };
        const response = await fetcher.fetch("https://httpbin.org/headers", {
            headers,
        });

        expect(!!response).toEqual(true);
        const json: any = response.asJSON();
        expect(json.headers.Foo).toEqual(`bar`);
    });

    it("should reject an error on non resolvable hosts", async () => {
        expect.assertions(1);
        await expect(fetcher.fetch("https://ajciodsapcjmpdscijdps.xxx")).rejects.not.toEqual(null);
    });
});
