import {Response} from "./Response";

/**
 * Factory class for Responses.
 */
export interface ResponseFactory {
    /**
     * Creates a new response.
     *
     * @param body The body of the response.
     * @param status The status of teh response.
     * @param contentType The content type of the response.
     */
    create(body: string, status: number, contentType: string | null): Response;
}
