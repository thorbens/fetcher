import MIMEType from "whatwg-mimetype";
import {Response} from "./Response";

/**
 * Implementation of a http response.
 * Provides the response body, the http status code, the content type and the mime type.
 */
export class HttpResponse implements Response {
    /**
     * The raw response body
     */
    public readonly body: string;
    /**
     * The HTTP status code.
     */
    public readonly status: number;
    /**
     * The content type set by the response server.
     */
    public readonly contentType: string | null;
    /**
     * The mime type set by the response server.
     */
    private readonly mimeType: MIMEType | null;

    public constructor(body: string, status: number, contentType: string | null) {
        this.body = body;
        this.status = status;
        this.contentType = contentType;
        this.mimeType = this.contentType ? new MIMEType(this.contentType) : null;
    }

    /**
     * @inheritDoc
     */
    public getMimeType(): MIMEType | null {
        return this.mimeType;
    }

    /**
     * @inheritDoc
     */
    public asJSON<T extends object>(): T {
        return JSON.parse(this.body);
    }
}
