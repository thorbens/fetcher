import axios, {AxiosRequestConfig} from "axios";
import {AbstractFetcher} from "./AbstractFetcher";
import {FetchOptions} from "./FetchOptions";
import {Response} from "./Response";

/**
 * Axios Implementation for a Fetcher.
 *
 * @see https://www.npmjs.com/package/axios
 */
export class AxiosFetcher extends AbstractFetcher {
    /**
     * @inheritDoc
     */
    public fetch(url: string, options?: FetchOptions): Promise<Response> {
        return this.performRequest(url, this.mapOptions(options || {}));
    }

    /**
     * Maps the given options to fetch options.
     *
     * @param options The options to map.
     */
    private mapOptions(options: FetchOptions): AxiosRequestConfig {
        const requestConfig: AxiosRequestConfig = {
            method: options.method || `get`,
            // disable json parsing, @see https://github.com/axios/axios/issues/907#issuecomment-373988087
            transformResponse: undefined,
            validateStatus: () => true,
        };
        if (options.headers) {
            requestConfig.headers = options.headers;
        }
        if (options.data) {
            requestConfig.data = options.data;
            requestConfig.headers = {
                "Content-Type": "application/json",
                ...requestConfig.headers, // merge with existing headers
            };
        }

        return requestConfig;
    }

    /**
     * Performs a request to the given url with the given options.
     *
     * @param url The url to fetch.
     * @param options The options to use.
     */
    private async performRequest(url: string, options: AxiosRequestConfig): Promise<Response> {
        const res = await axios.request({
            url,
            ...options,
        });
        const contentType = res.headers["content-type"] || null;
        const statusCode = res.status;
        const body = await res.data;
        const response = this.getResponseFactory().create(body, statusCode, contentType);
        if (response.status >= 400) {
            return Promise.reject(response);
        }

        return response;
    }
}
