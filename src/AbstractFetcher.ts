import {Fetcher} from "./Fetcher";
import {FetchOptions} from "./FetchOptions";
import {Response} from "./Response";
import {ResponseFactory} from "./ResponseFactory";

/**
 * Abstract Implementation of Fetcher which provides basic functionality.
 */
export abstract class AbstractFetcher implements Fetcher {
    /**
     * The response factory used to create Response objects.
     */
    private readonly responseFactory: ResponseFactory;

    public constructor(responseFactory: ResponseFactory) {
        this.responseFactory = responseFactory;
    }

    /**
     * @inheritDoc
     */
    public abstract fetch(url: string, options?: FetchOptions): Promise<Response>;

    /**
     * Returns the response factory for this fetcher.
     */
    protected getResponseFactory(): ResponseFactory {
        return this.responseFactory;
    }
}
