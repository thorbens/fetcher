[![Build Status](https://travis-ci.com/thorb3N/fetcher.svg?branch=master)](https://travis-ci.com/thorb3N/fetcher)
[![codecov](https://codecov.io/gh/thorb3N/fetcher/branch/master/graph/badge.svg)](https://codecov.io/gh/thorb3N/fetcher)

# Description
This package provides simple functional for performing http requests.
Responses can be retrieved as JSON or loaded into a `Document` (via JSDom).

Currently, two different fetchers are implemented (libs are peer dependencies):
* `NodeFetchFetcher` uses `node-fetch` as library.
* `RequestFetcher` uses default `request` as library.
* `AxiosFetcher` uses default `axios` as library.

# Use
```js
const fetcher = new RequestFetcher();
const response = await fetcher.fetch("http://google.com");
const body = response.body; // contains the raw response body
```

Post example:
```js
const fetcher = new RequestFetcher();
const options: FetchOptions = {
    data: {
        foo: `bar`,
    },
    method: Method.POST,
};
const response = await fetcher.fetch("https://httpbin.org/post", options);
const json = response.asJSON<HttpbinResponse>(); // contains the data returned as json object
```
